# Slim Transfer Learning

This repository uses the [TensorFlow-Slim image classification model library](https://github.com/tensorflow/models/tree/master/research/slim) for transfer learning.
A pretrained inception-v1 net, which was trained on the imagenet dataset, is finetuned to recognize individual classes. For demonstration, this code uses a [dataset of natural images by Intel](https://www.kaggle.com/puneet6060/intel-image-classification), which can be downloaded from Kaggle.

The used dataset consists of 6 classes:
1. buildings
2. forest
3. glacier
4. mountain
5. sea
6. street

A finetuning for 10000 steps has already been done on this dataset.

## How can I classify pictures?
The `classify-image.py` script is able to use the finetuned model for classification of a online picture. Simply change the url in the script for classification of other pictures. If you want to use offline pictures or classify a lot of pictures at once,  please have a look into the [classification examples](classification_examples) folder.

![](classification_examples/images/mountain.jpg)

Probability 88.33406%: (mountain)  
Probability 10.14899%: (glacier)  
Probability 1.48123%: (sea)  
Probability 0.01805%: (forest)  
Probability 0.00906%: (street)  
Probability 0.00861%: (buildings)

## How can I finetune the example dataset by myself?
Download the dataset from Kaggle, place it inside `datasets/natural/intel-image-classification.zip` and unzip it. Finetuning is done by executing the `finetune_natural.sh`. For training from scratch the current model inside `model` has to be removed first. Otherwise training will pick up from the latest checkpoint.

## How can I finetune with my own dataset?
Place your own dataset, consisting of pictures in folders with their class names, inside the `datasets` folder. Edit the files in the repository to match your dataset.
