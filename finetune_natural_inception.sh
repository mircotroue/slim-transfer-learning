#Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
#
# This script performs the following operations:
# 1. Downloads the Flowers dataset
# 2. Fine-tunes an InceptionV1 model on the Flowers training set.
# 3. Evaluates the model on the Flowers validation set.
#
# Usage:
# cd slim
# ./slim/scripts/finetune_inception_v1_on_flowers.sh
set -e

# Where the pre-trained InceptionV1 checkpoint is saved to.
PRETRAINED_CHECKPOINT_DIR=checkpoints

# Where the training (fine-tuned) checkpoint and logs will be saved to.
TRAIN_DIR=model

# Where the dataset is saved to.
DATASET_DIR=datasets/natural/seg_train

# Download the pre-trained checkpoint.
#if [ ! -d "$PRETRAINED_CHECKPOINT_DIR" ]; then
#  mkdir ${PRETRAINED_CHECKPOINT_DIR}
#fi
#if [ ! -f ${PRETRAINED_CHECKPOINT_DIR}/inception_v1.ckpt ]; then
#  wget http://download.tensorflow.org/models/inception_v1_2016_08_28.tar.gz
#  tar -xvf inception_v1_2016_08_28.tar.gz
#  mv inception_v1.ckpt ${PRETRAINED_CHECKPOINT_DIR}/inception_v1.ckpt
#  rm inception_v1_2016_08_28.tar.gz
#fi

# Download the dataset
#python download_and_convert_data.py \
    #  --dataset_name=flowers \
    #  --dataset_dir=${DATASET_DIR}

# Fine-tune only the new layers for 2000 steps.
python3 train_image_classifier.py \
      --train_dir=${TRAIN_DIR} \
        --dataset_name=natural \
          --dataset_split_name=train \
            --dataset_dir=${DATASET_DIR} \
              --model_name=inception_v1 \
                --checkpoint_path=${PRETRAINED_CHECKPOINT_DIR}/inception_v1.ckpt \
                  --checkpoint_exclude_scopes=InceptionV1/Logits \
                    --trainable_scopes=InceptionV1/Logits \
                      --max_number_of_steps=100000 \
                        --batch_size=64 \
                          --learning_rate=0.001 \
                            --save_interval_secs=60 \
                              --save_summaries_secs=60 \
                                --log_every_n_steps=1 \
                                  --optimizer=rmsprop \
                                    --weight_decay=0.00004 \
                                      --alsologotstderr \
                                        --clone_on_cpu=False
