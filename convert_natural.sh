#!/bin/bash

DATA_DIR=datasets/natural/seg_train
python3 download_and_convert_data.py --dataset_name=natural --dataset_dir="${DATA_DIR}"
