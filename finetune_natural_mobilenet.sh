PRETRAINED_CHECKPOINT_DIR=checkpoints

DATASET_DIR=datasets/natural/seg_train
TRAIN_DIR=model
#CHECKPOINT_DIR=checkpoints/mobilenet
#CHECKPOINT_FILE=${CHECKPOINT_DIR}/inception_v1.ckpt

python3 train_image_classifier.py \
    --max_number_of_steps=10000 \
    --learning-rate=0.0001 \
    --batch-size=256 \
    --train_dir=${TRAIN_DIR} \
    --dataset_name=natural \
    --dataset_split_name=train \
    --dataset_dir=${DATASET_DIR} \
    --model_name=mobilenet_v3_large \
    --clone_on_cpu=False \
    --alsologtostderr \
    --log_every_n_steps=1 \
    --checkpoint_path=${PRETRAINED_CHECKPOINT_DIR}/mobilenet/model-540000 \
    --checkpoint_exclude_scopes=MobilenetV3/Logits \
    --trainable_scopes=MobilenetV3/Logits
