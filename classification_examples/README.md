The images in this classification example are all taken from Wikimedia Commons:

- AgnosticPreachersKid - Eigenes Werk, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=9692738
- H2OMy, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=13743005
- Marianocecowski - Eigenes Werk, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=229556
- 名古屋太郎 - 投稿者が撮影。PENTAX K10D + smc PENTAX-A 1:1.2 50mm, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=10946701
- Hedwig Storch - Eigenes Werk, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=2936872
- CMEW, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=1402708
